import React from 'react';
import {
  BrowserRouter as Router,
  Switch,
  Route,
} from "react-router-dom";
import logo from './logo.svg';
import { Counter } from './features/counter/Counter';
import PrimeReact from 'primereact/api';

import './App.css';
import 'primereact/resources/primereact.min.css';
import 'primereact/resources/themes/bootstrap4-dark-blue/theme.css';
import 'primeicons/primeicons.css';




function App() {
  PrimeReact.ripple = true;

  return (
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
        </header>
        <Router>
          <div>
            <Switch>
              <Route path="/">
                <Counter />
              </Route>
            </Switch>
          </div>
        </Router>
      </div>
  );
}

export default App;
