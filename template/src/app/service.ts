// Credit to https://gist.github.com/paulsturgess/ebfae1d1ac1779f18487d3dee80d1258

import axios, {AxiosInstance} from "axios";
import { Redirect } from 'react-router-dom'


class Service {
    private service: AxiosInstance;
    
    constructor() {
        let service = axios.create({
            // headers: {csrf: 'token'},
            // baseURL: window.SERVER_ADDRESS,
        });
        service.interceptors.response.use(this.handleSuccess, this.handleError);
        this.service = service;
    }

    handleSuccess(response: any) {
        return response;
    }

    handleError = (error: { response: { status: any; }; }) => {
        switch (error.response.status) {
            case 401:
                window.location.href = "https://www.example.com/";
                break;
            case 404:
                window.location.href = "https://www.example.com/404";
                break;
            default:
                window.location.href = "https://www.example.com/500";
                break;
        }
        return Promise.reject(error)
    }


    async get(path: string, callback: (arg0: number, arg1: any) => any) {
        return await this.service.get(path).then(
            (response) => callback(response.status, response.data)
        );

    }

    async patch(path: string, payload: any, callback: (arg0: number, arg1: any) => any) {
        return await this.service.request({
            method: 'PATCH',
            url: path,
            responseType: 'json',
            data: payload
        }).then((response) => callback(response.status, response.data));
    }

    async post(path: string, payload: any, callback: (arg0: number, arg1: any) => any) {
        return await this.service.request({
            method: 'POST',
            url: path,
            responseType: 'json',
            data: payload
        }).then((response) => callback(response.status, response.data));
    }
}

export default new Service();
