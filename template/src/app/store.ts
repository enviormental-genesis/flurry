import { configureStore, combineReducers, MiddlewareArray } from '@reduxjs/toolkit'
import { useDispatch } from 'react-redux'


const rootReducer = combineReducers({})

export const store = configureStore({
  reducer: rootReducer,
  // middleware: new MiddlewareArray().concat(additionalMiddleware, logger)
})

export type AppDispatch = typeof store.dispatch
export const useAppDispatch = () => useDispatch<AppDispatch>() // Export a hook that can be reused to resolve types
export type RootState = ReturnType<typeof rootReducer>
